package imageprocessing;

import java.awt.image.BufferedImage;
import java.awt.Color;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import faceRecognition.Utils;
import Jama.Matrix;

public class Image {
	public static final int TYPE_BYTE_GRAY =10;
	public static final int TYPE_INT_RGB = 1; 
	
	BufferedImage imgBuff;
	String filePath = "";
	public BufferedImage getImgBuff() {
		return imgBuff;
	}


	public void setImgBuff(BufferedImage imgBuff) {
		this.imgBuff = imgBuff;
	}


	public String getFilePath() {
		return filePath;
	}


	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	public File getFile() {
		return file;
	}


	public void setFile(File file) {
		this.file = file;
	}

	File file;
	
	public Image(String filepath)
	{
		this.filePath = filepath;
		file = new File(this.filePath);
	}
	

	public void getBufferedImage()
	{
		try {
			imgBuff = ImageIO.read(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private double[][] getGrayImagePixelData()
	{
		double gray[][] = new double[imgBuff.getHeight()][imgBuff.getWidth()];
		
		for (int i = 0; i < imgBuff.getHeight(); i++)
		{
			for (int j = 0; j < imgBuff.getWidth(); j++)
			{
			    int rawRGB = imgBuff.getRGB(j, i);
			    int r, g, b;
			    int rgb[] = new int[]
			    		{
			    			(rawRGB >> 16) & 0xff, //red
			    			(rawRGB >> 8) & 0xff, //green
			    			(rawRGB) & 0xff //blue 
			    		};
			    
			    r = rgb[0];
			    g = rgb[1];
			    b = rgb[2];
			    gray[i][j] = (r + g + b) / 3;
			    //System.out.println(gray[i][j]);
			}
		}
		
		return gray;
	}

	public Matrix getGrayImageMatrix()
	{
		Matrix grayImageMatrix =  new Matrix(getGrayImagePixelData());
		return grayImageMatrix;
	}
	
	public static Matrix normalizeImage(Matrix imageDataMatrix)
	{
		double[][] d_imageDataMatrix = imageDataMatrix.getArray();
		double min = 255;
		double max = 0;
		for (double[] row : d_imageDataMatrix)
		{
			for (double val : row)
			{
				if (val < min)
				{
					min = val;
				}
				if ( val > max)
				{
					max = val;
				}
			}
		}
		double newMin = 0;
		double newMax = 255;
		
		for (double[] row : d_imageDataMatrix)
		{
			for (double val : row)
			{
				val = ((val - min)*((newMax-newMin)/(max - min))) + newMin;
			}
		}
		imageDataMatrix = new Matrix(d_imageDataMatrix);
		
		//imageDataMatrix = imageDataMatrix.timesEquals(1/imageDataMatrix.normF());
		return imageDataMatrix;
	}
	
	
	public static void printImage (double [][] imageMatrix, int width, int height, String imageName)
	{
		int [][] integerArrayImage = Utils.double2IntArray(imageMatrix);
		BufferedImage img = new BufferedImage(width, height, TYPE_INT_RGB);
		for ( int x = 0; x < width; x++ )
		{
			for (int y=0; y < height ; y++)
			{
				int grayLevel = integerArrayImage[y][x];
				int gray = (grayLevel << 16) + (grayLevel << 8) + grayLevel; 
		        img.setRGB(x, y, gray);
		      
			}
		}
		  File outputfile = new File("C:\\Users\\Sourabh\\Downloads\\yalefaces\\"+imageName+".png");
	        try {
				ImageIO.write(img, "png", outputfile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	 
	}
	
}
