package allmain;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import algorithms.AlgoTypes;
import faceRecognition.EigenFace;

public class Main_KNN {
	static String srcFileFolderPath ="C:\\Users\\Sourabh\\workspace\\SMAI_mini\\rsrc\\";
	public static int[][] confusionMatrix = new int[15][15];
	public static void main (String args[])
	{
			
			System.out.println("Training Face recognition Engine");
			//double startTime = System.currentTimeMillis();
			
			for (int i = 0; i < 5; i++)
			{
				EigenFace eigenface = new EigenFace();
				String TrainFileNames[] = new String [4];
				String TestFileNames[] = new String [1];
				TestFileNames[0] = srcFileFolderPath+i+".txt";
				for (int k = 0,j = 0 ; j < 5 ; j++)
				{
					if (j==i){
						continue;
					}
					
					TrainFileNames[k] = srcFileFolderPath+j+".txt";
					k++;
				}
				//eigenface.train(TrainFileNames, AlgoTypes.MLFFNN);
				eigenface.test(TestFileNames);
				try {
					BufferedWriter bw = new BufferedWriter(new FileWriter("transformedImages"+i+".csv"));
					double[][] transformedImage = eigenface.getTransformedImageMat().getArray();
					
					for (int row =0 ; row < transformedImage.length; row++)
					{
						for (int column = 0; column < transformedImage[row].length; column++)
						{
							bw.write(Double.toString(transformedImage[row][column])+",");
						}
						bw.write("\n");
					}
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("--------- Confusion Matrix ---------");
			for(int i=0;i<15;i++){
				for(int j=0;j<15;j++){
					System.out.print(Main_KNN.confusionMatrix[i][j] + ",");
				}
				System.out.println();
			}
	
	}
	
}
