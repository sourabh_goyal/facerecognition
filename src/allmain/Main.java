package allmain;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import neural.MultiLayerNN;
import common.Instance;
import common.Utils;
import faceRecognition.EigenFace;

public class Main {
	static String transformedTrainDataFileBasePath = "C:\\Users\\Sourabh\\workspace\\SMAI_mini\\rsrc\\data\\transformedTraining";
	static String transformedTestDataFileBasePath = "C:\\Users\\Sourabh\\workspace\\SMAI_mini\\rsrc\\data\\transformedTest";
	static String transformedAllDataFileBasePath = "C:\\Users\\Sourabh\\workspace\\SMAI_mini\\rsrc\\data\\transformedAll";
	static String srcFileFolderPath ="C:\\Users\\Sourabh\\workspace\\SMAI_mini\\rsrc\\";
	public static int[][] confusionMatrix = new int[15][15];
	
	private static String extractBaseImageName(String imgName){
		int dotIndex = imgName.indexOf('.');
		String baseName = "";
		if(dotIndex != -1){
			baseName = imgName.substring(0, dotIndex);
		}
		else{
			baseName = imgName;
		}
		return baseName;
	}
	
	//MLFFNN code
	public static void main(String args[]) throws Exception{
		for (int i = 0; i < 5; i++)
		{
			EigenFace eigenface = new EigenFace();
			String TrainFileNames[] = new String [4];
			String TestFileNames[] = new String [1];
			TestFileNames[0] = srcFileFolderPath+i+".txt";
			for (int k = 0,j = 0 ; j < 5 ; j++)
			{
				if (j==i){
					continue;
				}
				
				TrainFileNames[k] = srcFileFolderPath+j+".txt";
				k++;
			}
			eigenface.train(TrainFileNames);
			MultiLayerNN mlnn = new MultiLayerNN();
			mlnn.setNumClasses(eigenface.targetClasses.size());
			mlnn.buildClassifier(eigenface.getTrainingInstances(), eigenface.targetClasses.size());
			
			
			//TEST
			int correctPredictions = 0;
			int incorrectPredictions = 0;
			eigenface.setTestData(TestFileNames);
			List<Instance> testInstances = eigenface.getTestInstances();
			for(int i1=0;i1<testInstances.size();i1++){
				int predictedClassIndex = mlnn.predictClass(testInstances.get(i1));
				int actualClassIndex = testInstances.get(i1).classNumber();
				System.out.println(actualClassIndex + "-------" + predictedClassIndex);
				if(predictedClassIndex == actualClassIndex){
					correctPredictions++;
				}
				else{
					incorrectPredictions++;
				}
			}
			System.out.println("Correct Predictions - " + correctPredictions);
			System.out.println("Incorrect Predictions - " + incorrectPredictions);
		}
			
		
	}
	
	public static void main1 (String args[])
	{
			
			System.out.println("Training Face recognition Engine");
			//double startTime = System.currentTimeMillis();
			
			for (int i = 0; i < 5; i++)
			{
				EigenFace eigenface = new EigenFace();
				String TrainFileNames[] = new String [4];
				String TestFileNames[] = new String [1];
				TestFileNames[0] = srcFileFolderPath+i+".txt";
				for (int k = 0,j = 0 ; j < 5 ; j++)
				{
					if (j==i){
						continue;
					}
					
					TrainFileNames[k] = srcFileFolderPath+j+".txt";
					k++;
				}
				eigenface.train(TrainFileNames);
				eigenface.test(TestFileNames);
				BufferedWriter bw = null;
				try {
					bw = new BufferedWriter(new FileWriter(transformedAllDataFileBasePath+i+".csv"));
					String data = eigenface.getTransformedAllImagesData();
					bw.write(data);
					bw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally{
					try {
						bw.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			System.out.println("--------- Confusion Matrix ---------");
			for(int i=0;i<15;i++){
				for(int j=0;j<15;j++){
					System.out.print(Main.confusionMatrix[i][j] + ",");
				}
				System.out.println();
			}
	
	}
	
}
