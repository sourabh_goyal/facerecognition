package algorithms;

import org.opencv.core.Mat;
import org.opencv.ml.CvANN_MLP;
import org.opencv.ml.CvANN_MLP_TrainParams;

import Jama.Matrix;
import faceRecognition.Utils;

public class MLFFNN {

	CvANN_MLP mlp;
	CvANN_MLP_TrainParams mlp_trainParams;
	
	public MLFFNN (Mat layerSizes, int TrainParams)
	{
		mlp = new CvANN_MLP(layerSizes, CvANN_MLP.SIGMOID_SYM, 1, 1);
		mlp_trainParams = new CvANN_MLP_TrainParams();
		mlp_trainParams.set_train_method(TrainParams);
	}
	
	public void train(Matrix inputs, Matrix training_set_classifications)
	{
		Mat inputs_mat = Utils.jamaMat2OpenCVMat(inputs);
		Mat outputs = new Mat();
		mlp.train(inputs_mat, outputs, new Mat(), new Mat(), this.mlp_trainParams, 0);		
	}
	
	public Matrix test(Matrix testinput)
	{
		Mat testinput_mat = Utils.jamaMat2OpenCVMat(testinput);
		Mat output = new Mat();
		mlp.predict(testinput_mat, output);
		return Utils.openCVMat2JamaMat(output);
	}
}
