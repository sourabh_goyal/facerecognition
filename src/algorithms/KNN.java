package algorithms;

import java.util.ArrayList;

public class KNN {

	
	public static int getNearestNeighbor( double[][] dataMatrix, double [] testVector)
	{
		double EuclideanDistances[] = new double [testVector.length];
		//assuming each row of the inputMatrix is one vector
		for (int i = 0; i < dataMatrix.length; i++)
		{
			EuclideanDistances[i]=getEuclideanDistance(dataMatrix[i], testVector);
		}
		
		int NearestNeighbourIndex = getIndexOfMinEuclidean(EuclideanDistances);
		
		return NearestNeighbourIndex;	
	}
	
	
	public static int[] getKNearestNeighbors(double[][] dataMatrix, double [] testVector, int K)
	{
		ArrayList<Double> EuclideanDistances = new ArrayList<Double>(testVector.length);
				
		//assuming each row of the inputMatrix is one vector
		for (int i = 0; i < dataMatrix.length; i++)
		{
			EuclideanDistances.add(getEuclideanDistance(dataMatrix[i], testVector));
		}
		
		int KMinsIndice[] = new int [K];
		
		for (int i = 0; i < K; i++)
		{
			KMinsIndice[i] =getIndexOfMinEuclidean(EuclideanDistances);
			EuclideanDistances.remove(KMinsIndice[i]);				
		}
		
		return KMinsIndice;
	}
	
	private static double getEuclideanDistance(double[] datavector, double [] testVector)
	{
		double sumOfSquaresOfDifferences = 0;
		
		for (int i =0; i < testVector.length; i++)
		{
			sumOfSquaresOfDifferences = sumOfSquaresOfDifferences + Math.pow((testVector[i] - datavector[i]), 2);			
		}
		
		double EuclideanDistance = Math.pow(sumOfSquaresOfDifferences, 0.5);
		
		return EuclideanDistance;
	}
	
	private static int getIndexOfMinEuclidean(double[] EuclideanDistances)
	{
		
		double minEuclidean = EuclideanDistances[0];
		int indexOfMinEuclidean = 0;
		
		for (int i = 0; i < EuclideanDistances.length; i++)
		{
			if (EuclideanDistances[i] < minEuclidean)
			{
				indexOfMinEuclidean = i;
				minEuclidean = EuclideanDistances[i];
			}
		}
		
		return indexOfMinEuclidean;
	}
	
	private static int getIndexOfMinEuclidean(ArrayList<Double> EuclideanDistances)
	{
		
		double minEuclidean = EuclideanDistances.get(0);
		int indexOfMinEuclidean = 0;
		
		for (int i = 0; i < EuclideanDistances.size(); i++)
		{
			if (EuclideanDistances.get(i) < minEuclidean)
			{
				indexOfMinEuclidean = i;
				minEuclidean = EuclideanDistances.get(i);
			}
		}
		
		return indexOfMinEuclidean;
	}
	
}
