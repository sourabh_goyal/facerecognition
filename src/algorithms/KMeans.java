package algorithms;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import common.Utils;

public class KMeans {
	
	int k;
	List<List<Double>> dataSet;
	Map<Integer, String> dataIndexClassMap;
	Map<Integer, Integer> dataIndexClusterMap;
	Double[][] centroids;
	int[] clusterCount;
	String[] classesList;
	Map<String, Integer> classIndexMap;
	int[][] confusionMatrix;
	int iterations = 0;
	public KMeans(int k) {
		this.k = k;
		dataSet = new ArrayList<List<Double>>();
		clusterCount = new int[k];
	}
	
	public void runClusterer(String dataFileName) {
		importData(dataFileName);
		runClusterer();
	}
	
	private void resetStats(){
		confusionMatrix = new int[classesList.length][k];
		clusterCount = new int[k];
	}
	
	public void runClusterer() {
		resetStats();
		dataIndexClusterMap = new HashMap<Integer, Integer>();
		initializeMediods();
		boolean isStabilized = false;
		iterations = 0;
		while(true){
			isStabilized = !allocateClusters();
			if(isStabilized){
				break;
			}
			updateMediods();
			iterations++;
		}
		gatherStats();
		displayResult();
	}

	private void gatherStats() {
		Set<Entry<Integer, Integer>> clusterIndexEntrySet = dataIndexClusterMap.entrySet();
		for(Entry<Integer, Integer> clusterIndEntry : clusterIndexEntrySet){
			int dataIndex = clusterIndEntry.getKey();
			int clusterIndex = clusterIndEntry.getValue();
			int targetClassIndex = getClassIndex(dataIndex);
			confusionMatrix[targetClassIndex][clusterIndex]++;
			clusterCount[clusterIndex]++;
		}
	}
	
	private int getClassIndex(int dataIndex){
		return classIndexMap.get(dataIndexClassMap.get(dataIndex));
	}

	//Returns boolean which tells if there was any change to the cluster data points
	private boolean allocateClusters() {
		boolean anyChange = false;
		for(int i=0;i<dataSet.size();i++){
			List<Double> curInst = dataSet.get(i);
			int newClusterIndex = getCluster(curInst);
			boolean clusterChanged = false;
			if(dataIndexClusterMap.containsKey(i)){
				int oldClusterIndex = dataIndexClusterMap.get(i);
				if(oldClusterIndex != newClusterIndex){
					clusterChanged = true;
				}
			}
			else{
				clusterChanged = true;
			}
			if(clusterChanged){
				dataIndexClusterMap.put(i, newClusterIndex);
				anyChange = true;
			}
		}
		return anyChange;
	}

	private int getCluster(List<Double> instance) {
		Double[] inst = instance.toArray(new Double[instance.size()]);
		int clusterIndex = 0;
		double minDistance = Utils.getEuclideanDistance(inst, centroids[0]);
		for(int i=1;i<centroids.length;i++){
			double curDistance = Utils.getEuclideanDistance(inst, centroids[i]);
			if(curDistance < minDistance){
				minDistance = curDistance;
				clusterIndex = i;
			}
		}
		return clusterIndex;
	}

	private void initializeMediods() {
		int numInstances = dataSet.size();
		Random r = new Random();
		r.setSeed(System.currentTimeMillis());
		System.out.println("Initial Centroids : ");
		for(int i=0;i<k;i++){
			int mediodIndex = r.nextInt(numInstances);
			centroids[i] = dataSet.get(mediodIndex).toArray(centroids[i]);
			System.out.println("Cluster " + i + ": " + Arrays.toString(centroids[i]));
		}
		
		
		
	}

	private void updateMediods() {
		int numAtts = centroids[0].length;
		Double[] numDataPoints = new Double[k];
		Arrays.fill(numDataPoints, 0.0);
		Double[][] tempMediods = new Double[k][numAtts];
		for(int i=0;i<tempMediods.length;i++){
			Arrays.fill(tempMediods[i], 0.0);
		}
		Set<Entry<Integer, Integer>> clusterIndexEntrySet = dataIndexClusterMap.entrySet();
		for(Entry<Integer, Integer> clusterIndEntry : clusterIndexEntrySet){
			int dataIndex = clusterIndEntry.getKey();
			int clusterIndex = clusterIndEntry.getValue();
			Double[] instArr = dataSet.get(dataIndex).toArray(new Double[numAtts]);
			Utils.addArraysInPlace(tempMediods[clusterIndex], instArr);
			numDataPoints[clusterIndex]++;
		}
		
		for(int i=0;i<k;i++){
			for(int j=0;j<tempMediods[i].length;j++){
				tempMediods[i][j] /= numDataPoints[i];
			}
			centroids[i] = Arrays.copyOf(tempMediods[i], tempMediods[i].length);
		}
		
	}

	private void importData(String dataFileName) {
		Set<String> classSet = new HashSet<String>();
		dataIndexClassMap = new HashMap<Integer, String>();
		BufferedReader br;
		try {
			 br = new BufferedReader(new FileReader(dataFileName));
			 String curLine = "";
			 int index = 0;
			 int numAtts = 0;
			 while((curLine = br.readLine()) != null){
				 if(curLine.trim().length() == 0){
					 continue;
				 }
				 List<Double> dataEntry = new ArrayList<Double>();
				 String[] dataArr = curLine.split(",");
				 numAtts = dataArr.length-1;
				 for(int i=0;i<dataArr.length-1;i++){
					 dataEntry.add(Double.parseDouble(dataArr[i]));
				 }
				 dataSet.add(dataEntry);
				 String className = dataArr[dataArr.length-1].trim();
				 classSet.add(className);
				 dataIndexClassMap.put(index, className);
				 index++;
			 }
			 centroids = new Double[k][numAtts];
			 confusionMatrix = new int[classSet.size()][k];
			 index = 0;
			 classesList = new String[classSet.size()];
			 classIndexMap = new HashMap<String, Integer>();
			 for(String targetCls : classSet){
				 classesList[index] = targetCls;
				 classIndexMap.put(targetCls, index);
				 index++;
			 }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void displayResult() {
		int numInstances = dataSet.size();
		int numClasses = classesList.length;
		System.out.println("Num Interations: " + iterations);
		System.out.println("Cluster Centroids: ");
		for(int i=0;i<k;i++){
			System.out.println("Cluster " + i + ": " +Arrays.toString(centroids[i]));
		}
		System.out.println("\nClustered Instances");
		for(int i=0;i<k;i++){
			float percentage = (float)clusterCount[i]/numInstances*100;
			System.out.println("Cluster " + i + ": " +clusterCount[i] + "(" + percentage + "%)");
		}
		
		System.out.println("\nConfusion Matrix");
		for(int i=0;i<k;i++)
			System.out.print(i + "\t");
		System.out.println("\n");
		for(int i=0;i<numClasses;i++){
			for(int j=0;j<k;j++){
				System.out.print(confusionMatrix[i][j] + "\t");
			}
			System.out.println("| " + classesList[i]);
		}
	}

	public static void main(String args[]){
		KMeans km = new KMeans(3);
		km.importData("C:\\Users\\Sourabh\\workspace\\SMAI_mini\\rsrc\\data\\seeds\\seeds_dataset.txt");
		for(int i=0;i<5;i++){
			km.runClusterer();
		}
	}







	
}
