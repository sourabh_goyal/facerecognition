package algorithms;

import java.util.Random;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import common.Utils;

public class FarthestFirst {
	
	int k;
	List<List<Double>> dataSet;
	Map<Integer, String> dataIndexClassMap;
	Map<Integer, Integer> dataIndexClusterMap;
	Double[][] centroids;
	int[] clusterCount;
	String[] classesList;
	Map<String, Integer> classIndexMap;
	int[][] confusionMatrix;
	int iterations = 0;
	public FarthestFirst(int k) {
		this.k = k;
		dataSet = new ArrayList<List<Double>>();
		clusterCount = new int[k];
	}
	
	private void resetStats(){
		confusionMatrix = new int[classesList.length][k];
		clusterCount = new int[k];
	}
	
	public void runClusterer() {
		int n = dataSet.size();
		int numAtts = dataSet.get(0).size();
		resetStats();
		dataIndexClusterMap = new HashMap<Integer, Integer>();
		Random r = new Random(System.currentTimeMillis());
		boolean[] selected = new boolean[n];
		double[] minDistance = new double[n];

	    for(int i = 0; i<n; i++) minDistance[i] = Double.MAX_VALUE;

	    int firstI = r.nextInt(n);
	    centroids[0] = dataSet.get(firstI).toArray(new Double[numAtts]);
	    selected[firstI] = true;

	    updateMinDistance(minDistance,selected,centroids[0]);

	    for(int i = 1; i < k; i++) {
	      int nextI =  farthestAway(minDistance, selected);
	      centroids[i] = dataSet.get(nextI).toArray(new Double[numAtts]);
	      selected[nextI] = true;
	      updateMinDistance(minDistance,selected,centroids[i]);
	    }

	    allocateClusters();
		gatherStats();
		displayResult();
	}
	
	protected int farthestAway(double[] minDistance, boolean[] selected) {
	    double maxDistance = -1.0;
	    int maxI = -1;
	    for(int i = 0; i<selected.length; i++) 
	      if (!selected[i]) 
		if (maxDistance < minDistance[i]) {
		  maxDistance = minDistance[i];
		  maxI = i;
		}
	    return maxI;
	  }

	protected void updateMinDistance(double[] minDistance, boolean[] selected, 
			   Double[] center) {
		int numAtts = center.length;
		for(int i = 0; i<selected.length; i++){
			if (!selected[i]) {
				Double[] curInst = dataSet.get(i).toArray(new Double[numAtts]);
				double d = Utils.getEuclideanDistance(center,curInst);
				if (d<minDistance[i]) 
				  minDistance[i] = d;
			}
		}
	}
	
	private void gatherStats() {
		Set<Entry<Integer, Integer>> clusterIndexEntrySet = dataIndexClusterMap.entrySet();
		for(Entry<Integer, Integer> clusterIndEntry : clusterIndexEntrySet){
			int dataIndex = clusterIndEntry.getKey();
			int clusterIndex = clusterIndEntry.getValue();
			int targetClassIndex = getClassIndex(dataIndex);
			confusionMatrix[targetClassIndex][clusterIndex]++;
			clusterCount[clusterIndex]++;
		}
	}
	
	private int getClassIndex(int dataIndex){
		return classIndexMap.get(dataIndexClassMap.get(dataIndex));
	}

	//Returns boolean which tells if there was any change to the cluster data points
	private boolean allocateClusters() {
		boolean anyChange = false;
		for(int i=0;i<dataSet.size();i++){
			List<Double> curInst = dataSet.get(i);
			int newClusterIndex = getCluster(curInst);
			boolean clusterChanged = false;
			if(dataIndexClusterMap.containsKey(i)){
				int oldClusterIndex = dataIndexClusterMap.get(i);
				if(oldClusterIndex != newClusterIndex){
					clusterChanged = true;
				}
			}
			else{
				clusterChanged = true;
			}
			if(clusterChanged){
				dataIndexClusterMap.put(i, newClusterIndex);
				anyChange = true;
			}
		}
		return anyChange;
	}

	private int getCluster(List<Double> instance) {
		Double[] inst = instance.toArray(new Double[instance.size()]);
		int clusterIndex = 0;
		double minDistance = Utils.getEuclideanDistance(inst, centroids[0]);
		for(int i=1;i<centroids.length;i++){
			double curDistance = Utils.getEuclideanDistance(inst, centroids[i]);
			if(curDistance < minDistance){
				minDistance = curDistance;
				clusterIndex = i;
			}
		}
		return clusterIndex;
	}
	
	private void importData(String dataFileName) {
		Set<String> classSet = new HashSet<String>();
		dataIndexClassMap = new HashMap<Integer, String>();
		BufferedReader br;
		try {
			 br = new BufferedReader(new FileReader(dataFileName));
			 String curLine = "";
			 int index = 0;
			 int numAtts = 0;
			 while((curLine = br.readLine()) != null){
				 if(curLine.trim().length() == 0){
					 continue;
				 }
				 List<Double> dataEntry = new ArrayList<Double>();
				 String[] dataArr = curLine.split(",");
				 numAtts = dataArr.length-1;
				 for(int i=0;i<dataArr.length-1;i++){
					 dataEntry.add(Double.parseDouble(dataArr[i]));
				 }
				 dataSet.add(dataEntry);
				 String className = dataArr[dataArr.length-1].trim();
				 classSet.add(className);
				 dataIndexClassMap.put(index, className);
				 index++;
			 }
			 centroids = new Double[k][numAtts];
			 confusionMatrix = new int[classSet.size()][k];
			 index = 0;
			 classesList = new String[classSet.size()];
			 classIndexMap = new HashMap<String, Integer>();
			 for(String targetCls : classSet){
				 classesList[index] = targetCls;
				 classIndexMap.put(targetCls, index);
				 index++;
			 }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void displayResult() {
		int numInstances = dataSet.size();
		int numClasses = classesList.length;
		System.out.println("Num Interations: " + iterations);
		System.out.println("Cluster Centroids: ");
		for(int i=0;i<k;i++){
			System.out.println("Cluster " + i + ": " +Arrays.toString(centroids[i]));
		}
		System.out.println("\nClustered Instances");
		for(int i=0;i<k;i++){
			float percentage = (float)clusterCount[i]/numInstances*100;
			System.out.println("Cluster " + i + ": " +clusterCount[i] + "(" + percentage + "%)");
		}
		
		System.out.println("\nConfusion Matrix");
		for(int i=0;i<k;i++)
			System.out.print(i + "\t");
		System.out.println("\n");
		for(int i=0;i<numClasses;i++){
			for(int j=0;j<k;j++){
				System.out.print(confusionMatrix[i][j] + "\t");
			}
			System.out.println("| " + classesList[i]);
		}
	}
	
	public static void main(String args[]){
		FarthestFirst ff = new FarthestFirst(3);
		ff.importData("C:\\Users\\Sourabh\\workspace\\SMAI_mini\\rsrc\\data\\iris\\iris.data");
		for(int i=0;i<5;i++){
			ff.runClusterer();
		}
	}
}
