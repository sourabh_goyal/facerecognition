package faceRecognition;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;

import Jama.Matrix;

public class Utils {
	
	public static Matrix openCVMat2JamaMat(Mat M)
	{
		Size size_M = M.size();
		Matrix outputMatrix = new Matrix((int)size_M.height, (int) size_M.width);
		
		for (int i = 0; i < size_M.height; i++)
		{
			for ( int j = 0; j < size_M.width; j++)
			{
				outputMatrix.set(i, j, M.get(i, j)[0]);
			}
		}
		
		return outputMatrix;
	}
	

	public static Mat jamaMat2OpenCVMat(Matrix M)
	{
		
		Mat outputM =  new Mat(M.getRowDimension(), M.getColumnDimension(), CvType.CV_64F);
		
		for (int i = 0; i < M.getRowDimension(); i++)
		{
			for ( int j = 0; j < M.getColumnDimension(); j++)
			{
				outputM.put(i, j, M.get(i, j));
			}
		}
		
		return  outputM;
	}
	
	public static double[] mat2vec(Matrix M)
	{
		double d_M [][] = M.getArray();
		double vec []= new double[M.getColumnDimension()*M.getRowDimension()];
		int pos = 0;
		for (double row [] : d_M)
		{
			for (double val : row)
			{
				vec[pos]=val;
				pos++;
			}
		}
		return vec;
	}
	
	public static int[][] double2IntArray(double[][] inputarray)
	{
		int [][] outputArray =  new int [inputarray.length][inputarray[0].length];
		
		for (int i = 0; i < inputarray.length; i ++)
		{
			for (int j =0; j < inputarray[0].length; j++)
			{
				outputArray[i][j] = (int) inputarray[i][j];
			}
		}
		
		return outputArray;
	}
	
	public static double[][] singleArray2TwoDArray(double[] array, int numOfRows, int numOfColumns)
	{
		int pos = 0;
		double ouputarray[][] = new double[numOfRows][numOfColumns];
		for (int i =  0 ; i <numOfRows; i++)
		{
			for (int j =0 ; j < numOfColumns ; j++)
			{
				try {
					ouputarray[i][j] = array[pos];
					pos++;
				}
				catch (Exception E)
				{
					ouputarray[i][j]=255;
					pos++;
				}
			}
		}
		return ouputarray;
	}
	
}
