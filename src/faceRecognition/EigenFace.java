package faceRecognition;

import imageprocessing.Image;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import common.Instance;
import common.Utils;
import algorithms.KNN;
import allmain.Main;
import Jama.EigenvalueDecomposition;
import Jama.Matrix;

public class EigenFace {
	
	Matrix meanSubtractedImageVectors;
	Matrix meanSubtractedTestImageVectors;
	Matrix transformedImageMat;		//Where each row is the transformed image(n*d size matrix)
	Matrix transformedTestImagesMat;
	Matrix meanImageMatrix;
	Matrix faceSpace;
	HashMap<Integer, String> TrainSetMap = new HashMap<Integer, String>();	
	HashMap<Integer, String> TestingSetMap = new HashMap<Integer, String>();
	ArrayList<Matrix> Training_grayMatrices = new ArrayList<Matrix>();
	ArrayList<Matrix> Testing_grayMatricesMeanSubtracted = new ArrayList<Matrix>();
	String transformedTrainDataFileBasePath = "output\\data\\transformedTraining";
	String transformedTestDataFileBasePath = "output\\data\\transformedTest";
	public Set<String> targetClasses = new HashSet<String>();
	
	public String getTransformedAllImagesData(){
		StringBuilder sb = new StringBuilder(getTransformedTrainImagesData());
		sb.append("\n" + getTransformedTestImagesData());
		return sb.toString();
	}
	public String getTransformedTrainImagesData(){
		StringBuilder csString = new StringBuilder("");
		int index = 0;
		for(double[] obj : transformedImageMat.getArray()){
			String targetClass = extractBaseImageName(TrainSetMap.get(index));
			String str = Utils.getStringFromArray(obj) + "," + targetClass;
			csString.append(str + "\n");
			index++;
		}
		csString.deleteCharAt(csString.length()-1);
		return csString.toString();
	}
	
	public List<Instance> getTrainingInstances(){
		int index = 0;
		List<Instance> instances = new ArrayList<Instance>();
		for(double[] obj : transformedImageMat.getArray()){
			String targetClass = extractBaseImageName(TrainSetMap.get(index));
			int classNumber = Integer.parseInt(targetClass.substring(targetClass.length()-2));
			instances.add(new Instance(obj, targetClass, classNumber));
			index++;
		}
		return instances;	
	}
	
	public String getTransformedTestImagesData(){
		StringBuilder csString = new StringBuilder("");
		int index = 0;
		for(double[] obj : transformedTestImagesMat.getArray()){
			String targetClass = extractBaseImageName(TestingSetMap.get(index));
			String str = Utils.getStringFromArray(obj) + "," + targetClass;
			csString.append(str + "\n");
			index++;
		}
		csString.deleteCharAt(csString.length()-1);
		return csString.toString();
	}
		
	public List<Instance> getTestInstances(){
		int index = 0;
		List<Instance> instances = new ArrayList<Instance>();
		for(double[] obj : transformedTestImagesMat.getArray()){
			String targetClass = extractBaseImageName(TestingSetMap.get(index));
			int classNumber = Integer.parseInt(targetClass.substring(targetClass.length()-2));
			instances.add(new Instance(obj, targetClass, classNumber));
			index++;
		}
		return instances;	
	}
	
	public Matrix getTransformedTestImagesMat() {
		return transformedTestImagesMat;
	}
	
	public Matrix getTransformedImageMat() {
		return transformedImageMat;
	}

	public void setTransformedImageMat(Matrix transformedImageMat) {
		this.transformedImageMat = transformedImageMat;
	}

	public Matrix getMeanImageMatrix() {
		return meanImageMatrix;
	}


	String folderpathname = "C:\\Users\\Sourabh\\Downloads\\yalefaces\\";

	public void train(String trainingImagesFileNames[])
	{
		//File folder = new File(trainingImagesFolderPath);
		//File[] listOfFiles = folder.listFiles();
		ArrayList<Matrix> Training_grayMatrices = new ArrayList<Matrix>();
		Integer TrainFileIndex = 0;
		for (String ImagePathFile : trainingImagesFileNames)
		{
			File trainingFileName = new File (ImagePathFile);
			try {
				BufferedReader br = new BufferedReader( new FileReader(trainingFileName));
				String line = null;
				while (null!=(line= br.readLine()))
				{	String absfilePath = folderpathname+line;
					File file = new File (absfilePath);
					if (file.isFile())
				    {
				        //System.out.println("processing "+folder.getAbsolutePath()+"/"+file.getName());
				        Image img = new Image(folderpathname+line);
				        img.getBufferedImage();
				        Training_grayMatrices.add(img.getGrayImageMatrix());
				        TrainSetMap.put(TrainFileIndex, line);
				        String targetClass = extractBaseImageName(line);
				        targetClasses.add(targetClass);
				        TrainFileIndex++;
				    }
				}
			} catch (Exception e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
		/*for (File file : listOfFiles)
		{
		    if (file.isFile())
		    {
		        System.out.println("processing "+folder.getAbsolutePath()+"/"+file.getName());
		        Image img = new Image(folder.getAbsolutePath()+"/"+file.getName());
		        img.getBufferedImage();
		        grayMatrices.add(img.getGrayImageMatrix());
		    }
		}*/
		
		//EigenFace eigenFace = new EigenFace();
		
		Training_grayMatrices = this.normalizeImageMatrix(Training_grayMatrices);
		
		this.meanImageMatrix = this.getMeanImageGrayMatrix(Training_grayMatrices);
		
		ArrayList<Matrix> meanSubtractedImages = this.getDiffMatrices(Training_grayMatrices, this.meanImageMatrix);
		
		this.faceSpace = this.getFaceSpace(meanSubtractedImages);
						
		// each row of transformedImageMat is an transformed Image
		this.transformedImageMat  = meanSubtractedImageVectors.times(this.faceSpace);
		
		
	}
	
	public void setTestData(String[] ImageFilePaths){
		Integer testFileIndex = 0;
		for (String ImagePathFile : ImageFilePaths)
		{
			File trainingFileName = new File (ImagePathFile);
			try {
				BufferedReader br = new BufferedReader( new FileReader(trainingFileName));
				String line = null;
				while (null!=(line= br.readLine()))
				{
					line =line.trim();
					String absFilepath = folderpathname+line;
					File file = new File (absFilepath);
					if (file.isFile())
				    {
				        //System.out.println("processing "+folder.getAbsolutePath()+"/"+file.getName());
				        Image img = new Image(absFilepath);
				        img.getBufferedImage();
				 
				        Testing_grayMatricesMeanSubtracted.add( img.getGrayImageMatrix().minus(meanImageMatrix));
				        TestingSetMap.put(testFileIndex, line);
				        testFileIndex++;
				    }
				}
			} catch (Exception e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		int index = 0;
		double transformedTestImageVectors[][] = new double[Testing_grayMatricesMeanSubtracted.size()][];
		for (Matrix meanSubtractedTestImageGrayMat : Testing_grayMatricesMeanSubtracted)
		{
			double meanSubtractedtestImageGrayVector[][] = new double[1][];
			meanSubtractedtestImageGrayVector[0] = Utils.mat2vec(meanSubtractedTestImageGrayMat);
			meanSubtractedTestImageGrayMat = new Matrix(meanSubtractedtestImageGrayVector);
			Matrix transformedTestImageMat = meanSubtractedTestImageGrayMat.times(this.faceSpace);
			transformedTestImageVectors[index] = transformedTestImageMat.getArray()[0];
			index++;
		}
		transformedTestImagesMat = new Matrix(transformedTestImageVectors);
	}
	public void test(String[] ImageFilePaths)
	{
		Integer testFileIndex = 0;
		for (String ImagePathFile : ImageFilePaths)
		{
			File trainingFileName = new File (ImagePathFile);
			try {
				BufferedReader br = new BufferedReader( new FileReader(trainingFileName));
				String line = null;
				while (null!=(line= br.readLine()))
				{
					line =line.trim();
					String absFilepath = folderpathname+line;
					File file = new File (absFilepath);
					if (file.isFile())
				    {
				        //System.out.println("processing "+folder.getAbsolutePath()+"/"+file.getName());
				        Image img = new Image(absFilepath);
				        img.getBufferedImage();
				 
				        Testing_grayMatricesMeanSubtracted.add( img.getGrayImageMatrix().minus(meanImageMatrix));
				        TestingSetMap.put(testFileIndex, line);
				        testFileIndex++;
				    }
				}
			} catch (Exception e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
		//File testFile = new File(ImageFilePath);
		//Image testImg = new Image(testFile.getAbsolutePath());
		//testImg.getBufferedImage();
		//Matrix meanSubtractedtestImageGrayMat = (Image.normalizeImage(testImg.getGrayImageMatrix())).minusEquals(this.meanImageMatrix);
		//Matrix meanSubtractedtestImageGrayMat = testImg.getGrayImageMatrix().minusEquals(this.meanImageMatrix);
		int index = 0;
		int correctPredictions = 0;
		int incorrectPredictions = 0;
		//int[][] confusionMatrix = new int[15][15];
		double transformedTestImageVectors[][] = new double[Testing_grayMatricesMeanSubtracted.size()][];
		for (Matrix meanSubtractedTestImageGrayMat : Testing_grayMatricesMeanSubtracted)
		{
			double meanSubtractedtestImageGrayVector[][] = new double[1][];
			meanSubtractedtestImageGrayVector[0] = Utils.mat2vec(meanSubtractedTestImageGrayMat);
			meanSubtractedTestImageGrayMat = new Matrix(meanSubtractedtestImageGrayVector);
			Matrix transformedTestImageMat = meanSubtractedTestImageGrayMat.times(this.faceSpace);
			transformedTestImageVectors[index] = transformedTestImageMat.getArray()[0];
			int indexOfNearestNeighbor = KNN.getNearestNeighbor((this.transformedImageMat.getArray()), transformedTestImageVectors[index]);
			System.out.println("matched image id: " + indexOfNearestNeighbor);
			String testImageName = TestingSetMap.get(index);
			String predictedTrainImageName = TrainSetMap.get(indexOfNearestNeighbor);
			System.out.println("predictedImage: " + predictedTrainImageName+ ", Test Image : "+ testImageName);
			String testBaseName = extractBaseImageName(testImageName).trim();
			String trainBaseName = extractBaseImageName(predictedTrainImageName);
			int testClassIndex = Integer.parseInt(testBaseName.substring(testBaseName.length()-2));
			int trainClassIndex = Integer.parseInt(trainBaseName.substring(trainBaseName.length()-2));
			Main.confusionMatrix[testClassIndex-1][trainClassIndex-1]++;
			if(testBaseName.equalsIgnoreCase(trainBaseName)){
				correctPredictions++;
				System.out.println("correct prediction");
			}
			else{
				incorrectPredictions++;
				System.out.println("incorrect prediction");
			}
			index++;
		}
		transformedTestImagesMat = new Matrix(transformedTestImageVectors);
		System.out.println("Correct Predictions - " + correctPredictions);
		System.out.println("Incorrect Predictions - " + incorrectPredictions);
		
	}
	
	private String extractBaseImageName(String imgName){
		int dotIndex = imgName.indexOf('.');
		String baseName = "";
		if(dotIndex != -1){
			baseName = imgName.substring(0, dotIndex);
		}
		else{
			baseName = imgName;
		}
		return baseName;
	}
	
	private ArrayList<Matrix> normalizeImageMatrix(ArrayList<Matrix> grayMatrices)
	{
		ArrayList<Matrix> normalizedGrayMatrices = new ArrayList<Matrix>();

		for (Matrix grayMatrix : grayMatrices) {
			grayMatrix = Image.normalizeImage(grayMatrix);
			normalizedGrayMatrices.add(grayMatrix);
		}

		return normalizedGrayMatrices;
	}

	private Matrix getMeanImageGrayMatrix(ArrayList<Matrix> grayMatrices)
	{
		int meanHieght = 0;
		int meanWidth = 0;
		int sumHieght = 0;
		int sumWidth = 0;

		for (Matrix grayMatrix : grayMatrices)
		{
			sumHieght = sumHieght + grayMatrix.getRowDimension();
			sumWidth = sumWidth + grayMatrix.getColumnDimension();
		}

		meanHieght = sumHieght / grayMatrices.size();
		meanWidth = sumWidth / grayMatrices.size();

		Matrix meanImageGrayMatrix = new Matrix( new double[meanHieght][meanWidth]);

		for (Matrix grayMatrix : grayMatrices)
		{
			meanImageGrayMatrix = meanImageGrayMatrix.plusEquals(grayMatrix);
		}
		
		double multiplier = 1 / (double) grayMatrices.size();

		meanImageGrayMatrix = meanImageGrayMatrix.timesEquals(multiplier);
		meanImageGrayMatrix = Image.normalizeImage(meanImageGrayMatrix);
		Image.printImage(meanImageGrayMatrix.getArray(), meanImageGrayMatrix.getColumnDimension(), meanImageGrayMatrix.getRowDimension(), "MeanImage");
		
		return meanImageGrayMatrix;
	}

	private ArrayList<Matrix> getDiffMatrices( ArrayList<Matrix> grayMatrices, Matrix meanMatrix) 
	{
		ArrayList<Matrix> diffMatrices = new ArrayList<Matrix>();
		for (Matrix grayMatrix : grayMatrices)
		{
			Matrix diffMatrix = grayMatrix.minus(meanMatrix);
			diffMatrices.add(diffMatrix);
		}

		return diffMatrices;
	}

	private Matrix getFaceSpace( ArrayList<Matrix> diffGrayMatrices)
	{
		Matrix sampleGrayMatrix = diffGrayMatrices.get(0);
		double[][] allVectors= new double [diffGrayMatrices.size()][sampleGrayMatrix.getRowDimension()*sampleGrayMatrix.getColumnDimension()];
		int sampleNumber = 0;
		
		for (Matrix grayMatrix : diffGrayMatrices)
		{
			allVectors[sampleNumber] = Utils.mat2vec(grayMatrix);
			sampleNumber++;
		}
		
		this.meanSubtractedImageVectors = new Matrix (allVectors);
		Matrix meanSubtractedImageVectorsTranspose =  this.meanSubtractedImageVectors.transpose();
	
		Matrix scatterMatrix = this.meanSubtractedImageVectors.times(meanSubtractedImageVectorsTranspose);
		
		EigenvalueDecomposition evd = new EigenvalueDecomposition(scatterMatrix);
		
		@SuppressWarnings("unused")
		double eigenVal[] =  evd.getRealEigenvalues();
		Matrix eigenVecMat = evd.getV();
		
		Matrix ScatterEigenVecMat = meanSubtractedImageVectorsTranspose.times(eigenVecMat);
		
		/*double [][] ScatterEigenVecArray = ScatterEigenVecMat.transpose().getArray(); 
		
		for (int i =0 ; i < ScatterEigenVecArray.length; i++)
		{
			double[] eigenFace = ScatterEigenVecArray[i];
			double[][] eigenFace2D = Utils.singleArray2TwoDArray(eigenFace, 243, 320);
			Image.printImage(eigenFace2D, 320, 243, "eigen"+ i);
		}*/
		
		// scatterEigenVecMat is the facespace
		return ScatterEigenVecMat;
	}
}
