package common;

import java.util.ArrayList;
import java.util.Enumeration;

public class Instance {
	
  protected double[] m_AttValues;
  String m_classValue;
  int m_classNumber;

  public Instance(double[] attValues, String classValue, int classNumber) {
    m_AttValues = attValues;
    m_classValue = classValue;
    m_classNumber = classNumber;
  }

  public int numAttributes() {
    return m_AttValues.length;
  }

  public void setValue(int attIndex, double value) {
    m_AttValues[attIndex] = value;
  }

  public double[] toDoubleArray() {

    double[] newValues = new double[m_AttValues.length];
    System.arraycopy(m_AttValues, 0, newValues, 0, m_AttValues.length);
    return newValues;
  }

  public double value(int attIndex) {

    return m_AttValues[attIndex];
  }

  public String classValue() {
	  return m_classValue;
  }
  
  public int classNumber() {
	  return m_classNumber;
  }
  
}

