package common;

import java.math.BigDecimal;

import Jama.Matrix;

public class Utils {
	
	public static String getStringFromArray(double[] obj2){
		StringBuilder csString = new StringBuilder("");
		
		for(double obj : obj2){
			csString.append(new BigDecimal(obj).stripTrailingZeros().toPlainString() + ",");
		}
		csString.deleteCharAt(csString.length()-1);
		return csString.toString();
	}
	
	public static String getStringFromMatrix(double[][] ds){
		StringBuilder csString = new StringBuilder("");
		for(double[] obj : ds){
			String str = getStringFromArray(obj);
			csString.append(str + "\n");
		}
		csString.deleteCharAt(csString.length()-1);
		return csString.toString();
	}
	public static double[] mat2vec(Matrix M)
	{
		double d_M [][] = M.getArray();
		double vec []= new double[M.getColumnDimension()*M.getRowDimension()];
		int pos = 0;
		for (double row [] : d_M)
		{
			for (double val : row)
			{
				vec[pos]=val;
				pos++;
			}
		}
		return vec;
	}
	
	public static int[][] double2IntArray(double[][] inputarray)
	{
		int [][] outputArray =  new int [inputarray.length][inputarray[0].length];
		
		for (int i = 0; i < inputarray.length; i ++)
		{
			for (int j =0; j < inputarray[0].length; j++)
			{
				outputArray[i][j] = (int) inputarray[i][j];
			}
		}
		
		return outputArray;
	}
	
	public static double[][] singleArray2TwoDArray(double[] array, int numOfRows, int numOfColumns)
	{
		int pos = 0;
		double ouputarray[][] = new double[numOfRows][numOfColumns];
		for (int i =  0 ; i <numOfRows; i++)
		{
			for (int j =0 ; j < numOfColumns ; j++)
			{
				try {
					ouputarray[i][j] = array[pos];
					pos++;
				}
				catch (Exception E)
				{
					ouputarray[i][j]=255;
					pos++;
				}
			}
		}
		return ouputarray;
	}
	
	public static double getEuclideanDistance(Double[] inst, Double[] mediods)
	{
		double sumOfSquaresOfDifferences = 0;
		
		for (int i =0; i < mediods.length; i++)
		{
			sumOfSquaresOfDifferences = sumOfSquaresOfDifferences + Math.pow((mediods[i] - inst[i]), 2);			
		}
		
		double EuclideanDistance = Math.pow(sumOfSquaresOfDifferences, 0.5);
		
		return EuclideanDistance;
	}

	public static void addArraysInPlace(Double[] targetArr, Double[] instArr) {
		for(int i=0;i<targetArr.length;i++){
			targetArr[i] += instArr[i];
		}
		
	}

}
