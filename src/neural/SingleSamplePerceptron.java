package neural;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class SingleSamplePerceptron {
	
	float y[][] = {{1,1,6},{1,7,2},{1,8,9},{1,9,9},{1,4,8},{1,8,5},{-1,-2,-1},{-1,-3,-3},{-1,-2,-4},{-1,-7,-1},{-1,-1,-3},{-1,-5,-2}};		//All samples
	float y1[][] = {{1,1,6},{1,7,2},{1,8,9},{1,9,9},{1,4,8},{1,8,5},{1,2,1},{1,3,3},{1,2,4},{1,7,1},{1,1,3},{1,5,2}};		//All samples
	float a[]={0,0,0};
	float b = 0;		//margin
	float[] b1 = new float[y.length];
	float threshold = 0;
	
	//mode = 0 -> Single Sample
	//mode = 1 -> Single Sample With Margin
	//mode = 2 -> Relaxation With Margin
	//mode = 3 -> Widrow Hoff
	
	void train(int mode){
		b = (float) 2;
		threshold = (float) 0.2;	
		a[0] = 1;
		a[1] = 1;
		a[2] = 0;
		Arrays.fill(b1, 1);
		int k=0;		//iteration number
		float initLearnRate = 2;
		float learningRate = (float)2;
		int numSamples = y.length;
		boolean complete = false;
		System.out.println("Initial weight Vector - " + a[0] + "," + a[1] + "," + a[2]);
		System.out.println("Learning Rate - " + learningRate);
		System.out.println("Margin - " + b);
		System.out.println("Algorithm Used - " + (mode+1));
		Random rand = new Random(System.currentTimeMillis());
		do{
			int i;
			for(i=0;i<numSamples;i++){
				if(mode == 3){
					k++;
					learningRate = initLearnRate/k;
					a = updateWeights(a,b,y1[i],a.length, learningRate,mode);
					if(k>=1000){
						complete = true;
					}
				}
				else if(isMisclassified(a, y[i], a.length,mode)){
					k++;
					//System.out.println("Iteration Number - " + k);
					/*if(k%50 == 0){
						//System.out.println("Number of Iterations completed, checkpoint - " + k);
						if(learningRate > 0.4){
							learningRate -= 0.1;
						}
					}*/
					
					//System.out.println(i +"th sample misclassified");
					a = updateWeights(a,b,y[i],a.length, learningRate,mode);
					//System.out.println("Updated a - " + a[0] + "," + a[1] + "," + a[2]);
					if(areAllClassifiedCorrectly(mode)){
						complete = true;
						break;
					}
				}
			}
			
		}while(!complete);
		System.out.println("Training Completed in " + k + " iterations");
		System.out.println("Final value of 'a' is - " + a[0] + "," + a[1] + "," + a[2]);
		System.out.println();
	}
		
	float[] updateWeights(float[] a, float b, float[] y, int n, float learnRate, int mode){
		switch(mode){
		case 0:
		case 1:
			return updateSingleSample(a, y, n, learnRate);
		
		case 2:
			return updateRelaxation(b, a, y, n, learnRate);
		case 3:
			return updateLMS(b, a, y, n, learnRate);
		}
		return null;
	}
	
	private float[] updateLMS(float b, float[] a, float[] y, int n,
			float learnRate) {
		float[] ans = new float[n];
		float[] update =  new float[n];
		float dotProduct = getDotProduct(a, y, n);
		for(int i=0;i<y.length;i++){
			update[i] = (b1[i] - dotProduct)*y[i];
		}
		for(int i=0;i<n;i++){
			ans[i] = a[i]+(learnRate*update[i]);
		}
		return ans;
	}

	float[] updateSingleSample(float[] a, float[] y, int n, float learnRate){
		float[] ans = new float[n];
		for(int i=0;i<n;i++){
			ans[i] = a[i]+(learnRate*y[i]);
		}
		return ans;
	}
	
	float[] updateRelaxation(float b, float[] a, float[] y, int n, float learnRate){
		float[] ans = new float[n];
		float[] update =  new float[n];
		float r = (float) ((b - getDotProduct(a, y, n))/Math.pow(magnitude(y),2));
		for(int i=0;i<y.length;i++){
			update[i] = r*y[i];
		}
		for(int i=0;i<n;i++){
			ans[i] = a[i]+(learnRate*update[i]);
		}
		return ans;
	}
	
	float magnitude(float[] a){
		float mag = 0;
		for(int i=0;i<a.length;i++){
			mag += (a[i]*a[i]);
		}
		mag = (float) Math.sqrt(mag);
		return mag;
	}
	
	boolean areAllClassifiedCorrectly(int mode){
		List<Integer> misclassified = new ArrayList<Integer>();
		int numMisClassified = 0;
		for(int i=0;i<y.length;i++){
			if(isMisclassified(a, y[i], a.length,mode)){
				misclassified.add(i);
				numMisClassified++;
			}
		}
		//System.out.println("Num Misclassified - " + numMisClassified);
		/*if(numMisClassified <= 3){
			System.out.println("Found");
		}*/
		if(numMisClassified > 0){
			return false;
		}
		return true;
	}
	
	boolean isMisclassified(float a[], float y[], int n, int mode){
		float dotProduct = getDotProduct(a, y	, n);
		//System.out.println("Dot Product - " + dotProduct);
		switch(mode){
		case 0:
			if(dotProduct > 0){
				return false;
			}
			break;
		case 1:
			if(dotProduct >= b){
				return false;
			}
			break;
		case 2:
			if(dotProduct >= b){
				return false;
			}
			if(Math.abs(dotProduct-b) <= 0.01){
				return false;
			}
			break;
		case 3:
			break;
		}
		return true;
	}
	
	float getDotProduct(float a[], float b[], int n){
		float dotProduct = 0;
		for(int i=0;i<n;i++){
			dotProduct += a[i]*b[i];
		}
		return dotProduct;
	}
	
	public static void main(String args[]){
		SingleSamplePerceptron ssp = new SingleSamplePerceptron();
		ssp.train(0);
		ssp.train(1);
		ssp.train(2);
		ssp.train(3);
	}
	

}
