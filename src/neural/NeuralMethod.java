package neural;

import java.io.Serializable;

public interface NeuralMethod extends Serializable {
  
  double outputValue(NeuralNode node);
  double errorValue(NeuralNode node);
  void updateWeights(NeuralNode node, double learn, double momentum);

}
