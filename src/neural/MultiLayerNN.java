package neural;

import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import common.Instance;

public class MultiLayerNN {
  
 @SuppressWarnings("serial")
protected class NeuralEnd 
    extends NeuralConnection {
    
    /** 
     * the value that represents the instance value this node represents. 
     * For an input it is the attribute number, for an output, if nominal
     * it is the class value. 
     */
    private int m_link;
    
    /** True if node is an input, False if it's an output. */
    private boolean m_input;

    /**
     * Constructor
     */
    public NeuralEnd(String id) {
      super(id);

      m_link = 0;
      m_input = true;
      
    }
  
    /**
     * Call this to get the output value of this unit. 
     * @param calculate True if the value should be calculated if it hasn't 
     * been already.
     * @return The output value, or NaN, if the value has not been calculated.
     */
    public double outputValue(boolean calculate) {
     
      if (Double.isNaN(m_unitValue) && calculate) {
	if (m_input) {
	  m_unitValue = m_currentInstance.value(m_link);
	}
	else {
	  //node is an output.
	  m_unitValue = 0;
	  for (int noa = 0; noa < m_numInputs; noa++) {
	    m_unitValue += m_inputList[noa].outputValue(true);
	   
	  }
	}
      }
      return m_unitValue;
      
      
    }
    
    public double errorValue(boolean calculate) {
      
      if (!Double.isNaN(m_unitValue) && Double.isNaN(m_unitError) 
	  && calculate) {
	
		if (m_input) {
		  m_unitError = 0;
		  for (int noa = 0; noa < m_numOutputs; noa++) {
		    m_unitError += m_outputList[noa].errorValue(true);
		  }
		}
		else {
			if (m_currentInstance.classNumber() == m_link) {
		      m_unitError = 1 - m_unitValue;
		    }
		    else {
		      m_unitError = 0 - m_unitValue;
		    }
		}
     }
     return m_unitError;
    }
    
    
    /**
     * Call this to reset the value and error for this unit, ready for the next
     * run. This will also call the reset function of all units that are 
     * connected as inputs to this one.
     * This is also the time that the update for the listeners will be 
     * performed.
     */
    public void reset() {
      
      if (!Double.isNaN(m_unitValue) || !Double.isNaN(m_unitError)) {
	m_unitValue = Double.NaN;
	m_unitError = Double.NaN;
	m_weightsUpdated = false;
	for (int noa = 0; noa < m_numInputs; noa++) {
		m_inputList[noa].reset();
	}
      }
    }
    
    /**
     * Call this to have the connection save the current
     * weights.
     */
    public void saveWeights() {
      for (int i = 0; i < m_numInputs; i++) {
        m_inputList[i].saveWeights();
      }
    }
    
    /**
     * Call this to have the connection restore from the saved
     * weights.
     */
    public void restoreWeights() {
      for (int i = 0; i < m_numInputs; i++) {
        m_inputList[i].restoreWeights();
      }
    }
    
    
    /** 
     * Call this function to set What this end unit represents.
     * @param input True if this unit is used for entering an attribute,
     * False if it's used for determining a class value.
     * @param val The attribute number or class type that this unit represents.
     * (for nominal attributes).
     */
    public void setLink(boolean input, int val) throws Exception {
      m_input = input;
      
      if (input) {
    	  m_type = PURE_INPUT;
      }
      else {
    	  m_type = PURE_OUTPUT;
      }
      m_link = val;
      
    }
    
    /**
     * @return link for this node.
     */
    public int getLink() {
      return m_link;
    }

  }
    
  /** The training instances. */
  private List<Instance> m_instances;
  
  /** The current instance running through the network. */
  private Instance m_currentInstance;

  /** The output units.(only feeds the errors, does no calcs) */
  private NeuralEnd[] m_outputs;

  /** The input units.(only feeds the inputs does no calcs) */
  private NeuralEnd[] m_inputs;

  /** All the nodes that actually comprise the logical neural net. */
  private NeuralConnection[] m_neuralNodes;

  /** The number of classes. */
  private int m_numClasses = 0;
  
  /** The number of attributes. */
  private int m_numAttributes = 0; //note the number doesn't include the class.

  /** The next id number available for default naming. */
  private int m_nextId;

  /** The number of epochs to train through. */
  private int m_numEpochs;

  /** The number used to seed the random number generator. */
  private int m_randomSeed;

  /** The actual random number generator. */
  private Random m_random;

  /** A flag to state that a nominal to binary filter should be used. */
  private boolean m_useNomToBin;

  /** The string that defines the hidden layers */
  private String m_hiddenLayers;

  /** This flag states that the user wants the input values normalized. */
  private boolean m_normalizeAttributes;

  /** This is the learning rate for the network. */
  private double m_learningRate;

  /** This is the momentum for the network. */
  private double m_momentum;

  private boolean m_reset;
  
  private SigmoidUnit m_sigmoidUnit;
  
  public MultiLayerNN() {
    m_instances = null;
    m_currentInstance = null;
    m_outputs = new NeuralEnd[0];
    m_inputs = new NeuralEnd[0];
    m_numAttributes = 0;
    m_numClasses = 0;
    m_neuralNodes = new NeuralConnection[0];
    m_nextId = 0;
    
    m_random = null;
    m_sigmoidUnit = new SigmoidUnit();
    m_normalizeAttributes = true;
    m_useNomToBin = true;
    
    m_numEpochs = 500;
    m_randomSeed = 0;
    m_hiddenLayers = "a";
    m_learningRate = .3;
    m_momentum = .2;
    m_reset = true;
  }

  public void setReset(boolean r) {
    m_reset = r;
      
  }

  /**
   * @return The flag for reseting the network.
   */
  public boolean getReset() {
    return m_reset;
  }

  /**
   * @param a True if the attributes should be normalized (even nominal
   * attributes will get normalized here) (range goes between -1 - 1).
   */
  public void setNormalizeAttributes(boolean a) {
    m_normalizeAttributes = a;
  }

  /**
   * @return The flag for normalizing attributes.
   */
  public boolean getNormalizeAttributes() {
    return m_normalizeAttributes;
  }

  /**
   * @param f True if a nominalToBinary filter should be used on the
   * data.
   */
  public void setNominalToBinaryFilter(boolean f) {
    m_useNomToBin = f;
  }

  /**
   * @return The flag for nominal to binary filter use.
   */
  public boolean getNominalToBinaryFilter() {
    return m_useNomToBin;
  }

  /**
   * This seeds the random number generator, that is used when a random
   * number is needed for the network.
   * @param l The seed.
   */
  public void setSeed(int l) {
    if (l >= 0) {
      m_randomSeed = l;
    }
  }
  
  /**
   * @return The seed for the random number generator.
   */
  public int getSeed() {
    return m_randomSeed;
  }

  /**
   * The learning rate can be set using this command.
   * NOTE That this is a static variable so it affect all networks that are
   * running.
   * Must be greater than 0 and no more than 1.
   * @param l The New learning rate. 
   */
  public void setLearningRate(double l) {
    if (l > 0 && l <= 1) {
      m_learningRate = l;
    }
  }

  /**
   * @return The learning rate for the nodes.
   */
  public double getLearningRate() {
    return m_learningRate;
  }

  /**
   * The momentum can be set using this command.
   * THE same conditions apply to this as to the learning rate.
   * @param m The new Momentum.
   */
  public void setMomentum(double m) {
    if (m >= 0 && m <= 1) {
      m_momentum = m;
  
     }
  }
  
  /**
   * @return The momentum for the nodes.
   */
  public double getMomentum() {
    return m_momentum;
  }

  /**
   * This will set what the hidden layers are made up of when auto build is
   * enabled. Note to have no hidden units, just put a single 0, Any more
   * 0's will indicate that the string is badly formed and make it unaccepted.
   * Negative numbers, and floats will do the same. There are also some
   * wildcards. These are 'a' = (number of attributes + number of classes) / 2,
   * 'i' = number of attributes, 'o' = number of classes, and 't' = number of
   * attributes + number of classes.
   * @param h A string with a comma seperated list of numbers. Each number is 
   * the number of nodes to be on a hidden layer.
   */
  public void setHiddenLayers(String h) {
    String tmp = "";
    StringTokenizer tok = new StringTokenizer(h, ",");
    if (tok.countTokens() == 0) {
      return;
    }
    double dval;
    int val;
    String c;
    boolean first = true;
    while (tok.hasMoreTokens()) {
      c = tok.nextToken().trim();

      if (c.equals("a") || c.equals("i") || c.equals("o") || 
	       c.equals("t")) {
	tmp += c;
      }
      else {
	dval = Double.valueOf(c).doubleValue();
	val = (int)dval;
	
	if ((val == dval && (val != 0 || (tok.countTokens() == 0 && first)) && 
	     val >= 0)) {
	  tmp += val;
	}
	else {
	  return;
	}
      }
      
      first = false;
      if (tok.hasMoreTokens()) {
	tmp += ", ";
      }
    }
    m_hiddenLayers = tmp;
  }

  /**
   * @return A string representing the hidden layers, each number is the number
   * of nodes on a hidden layer.
   */
  public String getHiddenLayers() {
    return m_hiddenLayers;
  }

  
  /**
   * Set the number of training epochs to perform.
   * Must be greater than 0.
   * @param n The number of epochs to train through.
   */
  public void setTrainingTime(int n) {
    if (n > 0) {
      m_numEpochs = n;
    }
  }

  /**
   * @return The number of epochs to train through.
   */
  public int getTrainingTime() {
    return m_numEpochs;
  }
  
  /**
   * Call this function to place a node into the network list.
   * @param n The node to place in the list.
   */
  private void addNode(NeuralConnection n) {
    
    NeuralConnection[] temp1 = new NeuralConnection[m_neuralNodes.length + 1];
    for (int noa = 0; noa < m_neuralNodes.length; noa++) {
      temp1[noa] = m_neuralNodes[noa];
    }

    temp1[temp1.length-1] = n;
    m_neuralNodes = temp1;
  }

  /**
   * this will reset all the nodes in the network.
   */
  private void resetNetwork() {
    for (int noc = 0; noc < m_numClasses; noc++) {
      m_outputs[noc].reset();
    }
  }
  
  /**
   * This will cause the output values of all the nodes to be calculated.
   * Note that the m_currentInstance is used to calculate these values.
   */
  private void calculateOutputs() {
    for (int noc = 0; noc < m_numClasses; noc++) {	
      //get the values. 
      m_outputs[noc].outputValue(true);
    }
  }

  /**
   * This will cause the error values to be calculated for all nodes.
   * Note that the m_currentInstance is used to calculate these values.
   * Also the output values should have been calculated first.
   * @return The squared error.
   */
  private double calculateErrors() throws Exception {
    double ret = 0, temp = 0; 
    for (int noc = 0; noc < m_numAttributes; noc++) {
      //get the errors.
      m_inputs[noc].errorValue(true);
      
    }
    for (int noc = 0; noc < m_numClasses; noc++) {
      temp = m_outputs[noc].errorValue(false);
      ret += temp * temp;
    }    
    return ret;
    
  }

  /**
   * This will cause the weight values to be updated based on the learning
   * rate, momentum and the errors that have been calculated for each node.
   * @param l The learning rate to update with.
   * @param m The momentum to update with.
   */
  private void updateNetworkWeights(double l, double m) {
    for (int noc = 0; noc < m_numClasses; noc++) {
      //update weights
      m_outputs[noc].updateWeights(l, m);
    }

  }
  
  /**
   * This creates the required input units.
   */
  private void setupInputs() throws Exception {
    m_inputs = new NeuralEnd[m_numAttributes];
    for (int noa = 0; noa < m_numAttributes; noa++) {
      	m_inputs[noa] = new NeuralEnd("NeuralIN" + noa);
		m_inputs[noa].setLink(true, noa);
    }

  }

  /**
   * This creates the required output units.
   */
  private void setupOutputs() throws Exception {
  
    m_outputs = new NeuralEnd[m_numClasses];
    for (int noa = 0; noa < m_numClasses; noa++) {
      
      m_outputs[noa]= new NeuralEnd("NeuralON" + noa);
      m_outputs[noa].setLink(false, noa);
      NeuralNode temp = new NeuralNode(String.valueOf(m_nextId), m_random,
				       m_sigmoidUnit);
      m_nextId++;
      addNode(temp);
      NeuralConnection.connect(temp, m_outputs[noa]);
    }
 
  }

  private void setupHiddenLayer()
  {
    int val = 25;  //num of nodes in a layer
    int prev = 0; //used to remember the previous layer
      
    for (int nob = 0; nob < val; nob++) {
		NeuralNode temp = new NeuralNode(String.valueOf(m_nextId), m_random,
						 m_sigmoidUnit);
		m_nextId++;
		addNode(temp);
	}      

      for (int noa = 0; noa < m_numAttributes; noa++) {
    	  for (int nob = m_numClasses; nob < m_numClasses + val; nob++) {
    		  NeuralConnection.connect(m_inputs[noa], m_neuralNodes[nob]);
    	  }
      }
      for (int noa = m_neuralNodes.length - val; noa < m_neuralNodes.length; noa++) {
    	  for (int nob = 0; nob < m_numClasses; nob++) {
    		  NeuralConnection.connect(m_neuralNodes[noa], m_neuralNodes[nob]);
    	  }
      }
  }
  
  /**
   * Call this function to build and train a neural network for the training
   * data provided.
   * @param i The training data.
   * @throws Exception if can't build classification properly.
   */
  public void buildClassifier(List<Instance> instances, int numClasses) throws Exception {

    m_instances = null;
    m_currentInstance = null;
    m_outputs = new NeuralEnd[0];
    m_inputs = new NeuralEnd[0];
    m_numAttributes = 0;
    m_numClasses = numClasses;
    m_neuralNodes = new NeuralConnection[0];
    m_nextId = 0;
    m_instances = instances;
    m_random = new Random(m_randomSeed);
    m_numAttributes = m_instances.get(0).numAttributes();
     
    setupInputs();
      
    setupOutputs();    
    setupHiddenLayer();
    
    //connections done.
    
    double tempRate;
    
    for (int noa = 1; noa < m_numEpochs + 1; noa++) {
      for (int nob = 0; nob < m_instances.size(); nob++) {
    	  m_currentInstance = m_instances.get(nob);
    	  //this is where the network updating (and training occurs, for the
		  //training set
		  resetNetwork();
		  calculateOutputs();
		  tempRate = m_learningRate;  
		  updateNetworkWeights(tempRate, m_momentum);
	  }
    }
  }

  /**
   * Call this function to predict the class of an instance once a 
   * classification model has been built with the buildClassifier call.
   * @param i The instance to classify.
   * @return A double array filled with the probabilities of each class type.
   * @throws Exception if can't classify instance.
   */
  public double[] distributionForInstance(Instance i) throws Exception {

    m_currentInstance = i;
    
    resetNetwork();
    
    //since all the output values are needed.
    //They are calculated manually here and the values collected.
    double[] theArray = new double[m_numClasses];
    for (int noa = 0; noa < m_numClasses; noa++) {
      theArray[noa] = m_outputs[noa].outputValue(true);
    }
        
    //now normalize the array
    double count = 0;
    for (int noa = 0; noa < m_numClasses; noa++) {
        count += theArray[noa];
      }
    for (int noa = 0; noa < m_numClasses; noa++) {
      theArray[noa] /= count;
    }
    return theArray;
  }
  
  public int predictClass(Instance i) throws Exception{
	  double[] distributionArray = distributionForInstance(i);
	  int maxIndex = 0;
	  double maxVal = distributionArray[0];
	  for(int i1=1;i1<distributionArray.length;i1++){
		  if(distributionArray[i1] > maxVal){
			  maxVal = distributionArray[i1];
			  maxIndex = i1;
		  }
	  }
	  return maxIndex;
  }

	public void setNumClasses(int numClasses) {
		m_numClasses = numClasses;
	}
}

